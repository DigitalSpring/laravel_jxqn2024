<?php
// 2.3  视图
/**
 * 创建视图文件后在控制器中加载
 * **/
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyController extends Controller
{
     public function show()
    {
        //加载视图文件resources/views/home/test/show.blade.php
        // 写法一
        //return view('home/test/show');
        //写法二
        return view('home.test.show');
    }
    public function shop1()
    {
        $data=[
            'content'=>'文本'
        ];
        return view('home.test.show',$data);
    }
    //
    public function shop2()
    {
        return view('home.test.show')->with('content','<b>加粗</b>');
    }
    public function shop3()
    {
        return view('home.test.show')->with('time',time());    
    }
    public function shop4()
    {
        $data=[['id'=>1,'name'=>'TOME'],['id'=>2,'name'=>'Anye']];
        return view('show',['data'=>$data]);
    }
    public function shop5()
    {
        $week=date('N');
        return view('shop',['week'=>$week]);
    }
    public function shop()
    {
        return view('child');
    }
}
