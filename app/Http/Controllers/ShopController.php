<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function show()
    {
        return view('shop');
        // 写法一
        //return view('home/test/show');
        //写法二(新)
         // return view('home.test.show');
    }
}
