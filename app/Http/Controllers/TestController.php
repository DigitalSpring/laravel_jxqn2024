<?php
// 命名空间
namespace App\Http\Controllers;
// 导入request类
use Illuminate\Http\Request;
// 导入基类
class TestController extends Controller
{
    //代码空间
    public function input(Request $requset)
    {
        //接受url参数
        // $name=$requset->input('name');
        //接受路由参数
        $name=$requset->name;
        return  'name值为'.$name;
    }
    public function form()
    {
        return view('form');
    }
    public function transfer()
    {
        return '转帐成功';
    }
    public function profile()
    {
         // dump(session()-> all());
        return view('profile');
    }
    public function store(Request $requset)
    {
        // 验证规则
        $validatedDate =$requset ->validate([
            'name'=>'required|string|bail|max:10',
            'email'=>'required|email',
            'age'=>'required|integer',
            'hibby'=>'required'
        ],[
            'name.required'=>'姓名不能为空！',
            'email.email'=>'邮件格式错误',
            '*.required'=>'格式填写错误或没有填写0'
        ]);
    }
    public function testSeession()
    {
        //写入
        session(['name'=>'张三']);
        //判断有没有元素没有输出0
        dump(session('age',0));
        //输出结果
        dump(session('name'));
        //所有session
        dump(session()->all());
       //删除
        // dump(session()->forget('name'));
        //判断村寨
        dump(session()->has('name'));
        //删除所有
        session()->flush();
    }
}
