<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class UserController extends Controller
{
    //
    public function login()
    {
        return view('user.login');
    }
    public function check(Request $request)
    {
        $name=$request->input('name');
        $password=$request->input('password');
        $rule=[
            'name'=>'required',
            'password'=>'required'
        ];
        $message=[
            'name.required'=>'用户名不能为空',
            'password.required'=>'密码不能为空'
        ];
        $validator=Validator::make($request->all(),$rule,$message);
        if($validator->fails()){
            foreach ($validator->getMessageBog()->toArray() as $v){
                $msg=$v[0];
            }
            return $msg;
        }
        if($name!='admin'|| $password!='123456'){
            return '用户密码不正确';
        }
        session(['users'=>['id'=>1,'name'=>'admin']]);
        return '登录成功';
    }
    public function index()
    {
        return view('user.index');
    }
}
