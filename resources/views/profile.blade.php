<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="{{route('store')}}" method="post">
        姓名：<input type="text" name="name" id=""><br>
        邮箱：<input type="text" name="email" id=""><br>
        年龄：<input type="text" name="age" id=""><br>
        爱好： <input type="checkbox" name="hibby" id="">足球
        <input type="checkbox" name="hibby" id="">篮球
        <input type="checkbox" name="hibby" id="">排球<br>
        {{csrf_field()}}
        <input type="submit" value="保存">
    </form>

    @if ($errors->any())
    <!-- 获取第一条错误信息 -->
    {{$errors->first('name')}} <br>
    <!-- 获取指定字段错误信息 -->
    @foreach ($errors->get('name') as $message)
    {{$message}}
    @endforeach
<br>
<!-- 判断有没有包含字段的错误信息 -->
    @if ($errors->has('name'))
    姓名格式错误
    @endif
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
    @endif
</body>
</html>