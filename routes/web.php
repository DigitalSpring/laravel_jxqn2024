<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/shop', function () {
    return view('shop');
});
/*
Route::get('/hello',function(){
return 'hello';
});
*/
// 重定向
//Route::redirect('/text3','/');


// 通过match匹配get和post请求方式
Route::match(['get','post'],'text',function(){
    return '通过match匹配get和post请求方式';
    });
// 通过any匹配所有请求方式
Route::any('text1',function(){
    return '通过any匹配所有请求方式';
    });

// 路由带参数
Route::any('find/{id}',function($id){
    return '输入的ID为'.$id;
    });
    Route::any('find2/{id?}',function($id=0){
        return '输入的ID为'.$id;
        });

//别名
Route::get('/hello/123',function(){
    return 'hello';
    })->name('hello');

// 路由分组
Route::group(['prefix'=>'admin'],function(){
    Route::get('login',function(){
        return '这是/admin/login';
    });
    Route::get('logout',function(){
        return '这是/admin/logout';
    });
    Route::get('index',function(){
        return '这是/admin/index';
    });
});

// 控制器路由
Route::get('admin/text1','Admin\TestController@text1');
// Route::get('text/input','TestController@input');
Route::get('text/input/{name}','TestController@input');
Route::get('text1/input/{name}','Admin\MyController@input');
// 视图路由
Route::get('text1/index','MyController@shop');
Route::get('shop/index','ShopController@show');


// 表单认证
Route::get('test/form','TestController@form')->middleware('test');
Route::post('test/transfer','TestController@transfer')->name('trans');

Route::get('test/profile','TestController@profile');
Route::post('test/store','TestController@store')->name('store');

Route::get('test/testSeession','TestController@testSeession');

Route::group(['middleware'=>['test']],function(){
    Route::get('/',function(){
        return view('welcome');
    });
});

//php artisan make:middleware Test


//登录

Route::get('user/login','UserController@login');
Route::post('user/check','UserController@check')->name('check');
Route::get('user/index','UserController@index')->middleware('user');